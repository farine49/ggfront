import React, { useState} from 'react';
import { Link } from 'react-router-dom';
import './NavbarLogin.css';

function NavbarLogin() {
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  return (
    <>
      <nav className='NavbarLogin'>
        <div className='NavbarLogin-container'>
        <i class='fas fa-dragon' />
          <Link to='/' className='NavbarLogin-logo' onClick={closeMobileMenu}>
            GGCMarket
          </Link>
          <div className='menu-icon' onClick={handleClick}>
            <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
          </div>
         
        
        </div>
      </nav>
    </>
  );
}

export default NavbarLogin;
