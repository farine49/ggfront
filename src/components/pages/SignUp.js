import React from 'react';
import '../../App.css';
import NavbarLogin from '../NavbarLogin';
import CardsLogin from '../CardsLogin';
import Footer from '../Footer';


export default function SignUp() {
  return (
    <>
      <NavbarLogin />
      <CardsLogin />
      <Footer />
    </>
  )
}
