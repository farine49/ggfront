import React from 'react';
import '../../App.css';
import Navbar from '../Navbar';
import Cards from '../Cards';
import CardsPayment from '../CardsPayment';
import CardsWork from '../CardsWork';
import HeroSection from '../HeroSection';
import Footer from '../Footer';

function Home() {
  return (
    <>
      <Navbar />
      <HeroSection />
      <Cards />
      <CardsPayment />
      <CardsWork />
      <Footer />
    </>
  );
}

export default Home;
