import React from 'react';
import './Cards.css';
import CardItem from './CardItem';

function CardsPayment() {
  return (
    <div className='cards_payment'>
      <div className='cards_payment'>
      <h1 class="h1_black">Payment Methods</h1>
      <center><h2 class="h2_grey"><small>Find out how to pay in the most convenient way.</small></h2></center>
      </div>
      <div className='cards__container_payment'>
        <div className='cards__wrapper_payment'>
         
          <ul className='cards__items_payment'>
           
            <CardItem
              src='images/pay.png'
              label='More methods available & Secure Transactions by PaymentWall'
              styleName='_payment'
            />
         
          </ul>
        </div>
      </div>
    </div>
  );
}

export default CardsPayment;
