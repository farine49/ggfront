import React from 'react';
import './Footer.css';
import { Link } from 'react-router-dom';

function Footer() {
  return (
    <div className='footer-container'>
      <section className='footer-subscription'>
        <p className='footer-subscription-heading'>
          Join us on Discord
        </p>
        <div class='social-icons'>
            <Link
              class='social-icon-link discord'
              to='/'
              target='_blank'
              aria-label='Discord'
            >
              <i class='fab fa-discord' />
            </Link>
           
           
          </div>
        
      </section>
      <div class='footer-links'>
        <div className='footer-link-wrapper'>
          <div class='footer-link-items'>
            <h2>Ressources</h2>
            <Link to='/sign-up'>How to Buy</Link>
            <Link to='/'>Privacy Policy</Link>
            <Link to='/'>Terms of Service</Link>
          </div>
          <div class='footer-link-items'>
            <h2>Contact Us</h2>
            <Link to='/'>Help Center</Link>
            <Link to='/'>How to Sell</Link>
            <Link to='/'>Commission Fee</Link>
          </div>
        </div>
        
      </div>
      <section class='social-media'>
        <div class='social-media-wrap'>
          <div class='footer-logo'>
            <Link to='/' className='social-logo'>
              GGCMarket
              <i class='fas fa-dragon' />
            </Link>
          </div>
          <small class='website-rights'>GGCMarket Corporation © 2022</small>
       
        </div>
      </section>
    </div>
  );
}

export default Footer;
