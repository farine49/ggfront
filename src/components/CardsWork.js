import React from 'react';
import './Cards.css';
import CardItem from './CardItem';

function CardsPayment() {
  return (
    <div className='cards_work'>
      <div className='cards_work'>
      <h1 class="h1_black">How It Work</h1>
      </div>
      <div className='cards__container_work'>
        <div className='cards__wrapper_work'>
         
          <ul className='cards__items_work'>
           
            <CardItem
              src='images/pay.png'
          
              styleName='_work'
            />
         
          </ul>
        </div>
      </div>
    </div>
  );
}

export default CardsPayment;
