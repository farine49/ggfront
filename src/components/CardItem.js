import React from 'react';


function CardItem(props) {
  return (
    <>
      
        <div className={ "cards__item__link" + ( props.styleName || "") }>
          <figure className={ "cards__item__pic-wrap" + ( props.styleName || "") } data-category={props.label}>
            <img
              className={ "cards__item__img" + ( props.styleName || "") }
              alt='Travel'
              src={props.src}
            />
             
            
          </figure>
          <div className={ "cards__item__info" + ( props.styleName || "") }>
            <h5 className={"cards__item__text" + ( props.styleName || "") }>{props.text}</h5>
          </div>
        </div>
      
    </>
  );
}

export default CardItem;
