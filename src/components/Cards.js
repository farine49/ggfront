import React from 'react';
import './Cards.css';
import CardItem from './CardItem';

function Cards() {
  return (
    <div className='cards'>
      
      <div className='cards__container'>
        <div className='cards__wrapper'>
         
          <ul className='cards__items'>
            <CardItem
              src='images/shield.svg'
              text='Feel confident each time you transact with us. GamerProtect comes with SSL protection and wide range of payment processors under a safe and secured platform.'
              label='Secure Transactions'
        
              styleName=''
              
            />
            <CardItem
              src='images/best.svg'
              text='GGC provides competitive pricing to the buyers driven by a free market economy while striving to keep the cost low for our sellers.'
              label='Best Offers On Private Servers'
          
              styleName=''
            />
            <CardItem
              src='images/support.svg'
              text='Our dedicated Customer Service team are available to help with any queries about your orders and provide exceptional after-sales support.'
              label='Discord Support'
            
              styleName=''
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;
